from practice.wait_test_code import People
import unittest
import HTMLTestRunner


class TestPeople(unittest.TestCase):
    def setUp(self):
        print("Test is starting...")
        self.person = People(3, 4)

    def test_add(self):
        sum_1 = self.person.sum()
        self.assertEqual(sum_1, 7, 'add fail')

    def test_multi(self):
        multi_1 = self.person.multi()
        self.assertEqual(multi_1, 12, 'multi fail')

    def tearDown(self):
        print("Test end")


if __name__ == "__main__":
    # 构造测试集
    suite = unittest.TestSuite()
    suite.addTests([TestPeople("test_add"), TestPeople("test_multi")])

    # 将结果写入本地文件
    # with open('D:\\test_rusult\\test1.txt', "w") as UnitTestReport:
    #     # 执行测试
    #     runner = unittest.TextTestRunner(stream=UnitTestReport, verbosity=2)
    #     runner.run(suite)

    # 将结果写入HTML
    with open('D:\\test_rusult\\pyresult_1.html', 'wb') as UnitTestReport:
        runner = HTMLTestRunner.HTMLTestRunner(stream=UnitTestReport, title=u'测试报告', description=u'单元测试HTML型报告')
        runner.run(suite)
