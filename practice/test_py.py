import pytest
import allure
from practice.wait_test_code import People


class TestPeople(object):
    def test_add(self):
        person = People(3, 4)
        result = person.sum()
        assert result == 0


if __name__ == "__main__":
    pytest.main(['-s', '-q', '--alluredir', './report/xml'])
