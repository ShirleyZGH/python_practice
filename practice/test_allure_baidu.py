# _*_ coding=utf-8 _*_
# @Time   : 2021/2/8
# @Author : ShirleyZGH

import time
import logging

import allure
import pytest
from selenium import webdriver

# logging.basicConfig(level=logging.DEBUG)


@allure.feature("百度搜索")
@pytest.mark.parametrize('test_data1', ['allure', 'pytest', 'unittest'])
def test_baidu_demo(test_data1):
    with allure.step("打开百度网址"):
        driver = webdriver.Chrome()
        driver.get("http://www.baidu.com")

    with allure.step("搜索关键词"):
        driver.find_element_by_id("kw").send_keys(test_data1)
        time.sleep(2)
        driver.find_element_by_id("su").click()
        time.sleep(2)

    with allure.step("保存图片"):
        allure.attach('<head></head><body>首页</body>', 'Attach with HTML type', allure.attachment_type.HTML)
        driver.save_screenshot(r"D:\pycharm_prac\report\result\b.png")
        allure.attach.file(r"D:\pycharm_prac\report\result\b.png", attachment_type=allure.attachment_type.PNG)

    with allure.step("退出浏览器"):
        driver.quit()

    # class TestObject:
    # @classmethod
    # def setup_class(cls):
    #     logging.info("start!")
    # @classmethod
    # def teardown_class(cls):
    #     logging.info("end!")

