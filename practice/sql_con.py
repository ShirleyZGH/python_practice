import pymysql

localhost = "test-mysql-master.heyteago.com"
user = "heytea_testgroup"
password = "SH80x!F#"
port = 3306
db = pymysql.connect(host=localhost, user=user, password=password, db="test_go_1_production", port=port, charset='utf8')
cur = db.cursor()
sql_1 = "select * from orders order by id desc limit 10"
sql_2 = "select id,total_fee from orders order by id desc limit 10"
try:
    """  
    获取所有数据，遍历获取元组中索引值为0的项
    cur.execute(sql_1)
    res = cur.fetchall()
    for i in res:
        userid = i[0]
        print(userid)
    """

    # cur.execute(sql_2)
    cur.execute(sql_2)
    result = cur.fetchall()             # fetchall用法
    # for i in result:
    #     print(i)            # 遍历result中所有的元组数据
    for userid, pay in result:
        print("id为{}的顾客花费了{}元！".format(userid, pay))

    # for i in range(10):
    #     one, two = cur.fetchone()         # fetchone用法
    #     print("id为{}的顾客花费了{}元！".format(one, two))
    # one, two = result.split
    # print(cur.rownumber)
    # print(cur.fetchmany(2))

except Exception as err_msg:
    print(err_msg)


cur.close()
db.close()
