# -*- coding: utf-8 -*-
# @Time    : 2022/4/9 13:19
# @Author  : ShirleyZGH
# @FileName: python100_2.py
# @Software: PyCharm

# CRAPS赌博游戏
"""
CRAPS又称花旗骰，是美国拉斯维加斯非常受欢迎的一种的桌上赌博游戏。该游戏使用两粒骰子，玩家通过摇两粒骰子获得点数进行游戏。
简单的规则是：玩家第一次摇骰子如果摇出了7点或11点，玩家胜；玩家第一次如果摇出2点、3点或12点，庄家胜；
其他点数玩家继续摇骰子，如果玩家摇出了7点，庄家胜；如果玩家摇出了第一次摇的点数，玩家胜；其他点数，玩家继续要骰子，直到分出胜负。

Craps赌博游戏
我们设定玩家开始游戏时有1000元的赌注
游戏结束的条件是玩家输光所有的赌注
"""
from random import randint

money = 1000

while money:
    print("资金： %d 元" % money)
    debet = int(input("输入你的赌注："))
    if debet<0 or debet>money:
        print("你的赌资不够，请重新下注")
    else:
        sum_1=randint(1,6)+randint(1,6)
        print("第 1 次摇色子：%d" % sum_1)
        if sum_1 in (7,10):
            money += debet
            print("玩家胜利！")
        elif sum_1 in (2,3,12):
            money -= debet
            print("庄家胜利")
        else:
            flag=1
            while flag:
                sum_2=0
                sum_2=randint(1,6)+randint(1,6)
                flag += 1
                print("第 %d 次摇色子：%d" % (flag,sum_2))
                if sum_2==7:
                    money -= debet
                    print("庄家胜利")
                    flag=0
                elif sum_2==sum_1:
                    money += debet
                    print("玩家胜利！")
                    flag=0

print("----------------------------------------\n没钱了，你输了！！！\n----------------------------------------")

