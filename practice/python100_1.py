"""
不使用分支结构求0，100的偶数和
Date: 2022-04-08
"""
# sum_num =0
# for i in range(0,101,2):
#     print(i)
#     sum_num += i
#
# print(sum_num)

# 使用分支结构求0，100的偶数和 Date: 2022-04-08
# sum_1=0
# for i in range(1,101):
#     if i % 2 == 0:
#         sum_1 += i
#
# print(sum_1)

# 猜(0,100)之间的随机数
# import random
# random_num = random.randint(0,100)
# input_num = int(input("please input a num:"))
# while random_num != input_num:
#     if random_num > input_num:
#         print("smaller")
#     elif random_num < input_num:
#         print("bigger")
#     input_num = int(input("please input a num:"))
#
# print("guess right!")

# 猜(0,100)之间的随机数,break控制跳出循环
# import random
# random_num = random.randint(0,100)
# count_num =0
# while True:
#     input_num = int(input("please input a num:"))
#     count_num += 1
#     if random_num > input_num:
#         print("smaller")
#     elif random_num < input_num:
#         print("bigger")
#     else:
#         print("guess right!")
#         break
# print("猜了 %d 次" % count_num)

# 不使用分支结构求（1，100）的奇数和
# sum_old=0
# for i in range(1,100,2):
#     print(i)
#     sum_old+=i

# 输入三条边，能构成三角形则计算周长和面积
# a,b,c = eval(input("请输入三条边:"))
# if a+b>c and a+c>b and b+c>a:
#     cirle_tri = a+b+c
#     print('周长是: %d' % cirle_tri)
#     s = cirle_tri/2
#     square_tri = (s*(s-a)*(s-b)*(s-c))**0.5
#     print('面积是: %d' % square_tri)
# else:
#     print("不是三角形")


# 使用while语句实现（1，100）的奇数和
# i=1
# while(i<101):
#     print(i)
#     sum_old += i
#     i +=2
#
# print(sum_old)

# 去除重复数据
# list_1 = [1,1,1,2,3,4,5,3,2,2,4,5,6,7,7,8,8,5,4,3]
# c = list_1.count(1)
# print(c)
# set_1 = list(set(list_1))
# print(set_1)


# li_1 = [5,6,3,2]
# li_1.insert(1,0)
# print(li_1)

# 1-10所有偶数平方后返回
# li_3 = [i for i in range(1,11,2)]
#
# print(li_3)

# 输出九九乘法表
# for i in range(1,10):
#     for j in range(i,10):
#         print(" %d * %d = %d" % (i,j,i*j))

# 判断一个正整数是不是素数
# 方法一：效率最低，判断input_num能不能被小于它的数整除
# input_num = int(input("请输入一个整数："))
# if(input_num>1):
#     for i in range(2,input_num):
#         if(input_num%i==0):
#             print("%d 不是质数" % input_num)
#             break
#         else:
#             print("%d 是质数" % input_num)
# else:
#     print("%d 不是质数" % input_num)

# 方法二：假如n是合数，必然存在非1的两个约数p1和p2，其中p1<=sqrt(n)，p2>=sqrt(n)
# import math
#
# input_num = int(input("请输入一个整数："))
# n = int(math.sqrt(input_num))
# k = 1
# if input_num == 1:
#     k = 0
# elif(input_num>3):
#     for i in range(2,n+1):
#         if(input_num%i==0):
#             k = 0
#             break
# if k == 0:
#     print("%d 不是质数" % input_num)
# elif k == 1:
#     print("%d 是质数" % input_num)

# 打印如下所示的三角形图案
# row_num = int(input("输入想打印的行数:"))
# for i in range(1,row_num+1):
#     print("*" * i)

# for i in range(1,row_num+1):
#     print(" " * (row_num-i) + "*" * i)


# for i in range(1,row_num+1):
#     print("i为：%d, 空格：%d, 星星： %d" % (i,row_num-i,2*i-1))
#     print(" " * (row_num-i) + "*" * (2*i-1))

# 寻找水仙花数,它是一个3位数，该数字每个位上数字的立方之和正好等于它本身，例如：$1^3 + 5^3+ 3^3=153$。
# 方法一：自己写的  [153, 371]--错误原因：范围有误. 个位/十位应该从0开始计算
# math_self=[]
# math_self = [(100*a+10*b+c) for a in range(1,10)
#              for b in range(0,10)
#              for c in range(0,10)
#              if a**3+b**3+c**3 == 100*a+10*b+c]
# print(math_self)

# 方法二：自己写的  [153, 370, 371, 407]--这个是对的
# for self_num in range(100,1000):
#     a=self_num//100
#     b=self_num//10%10
#     c=self_num%10
#     count_num=a**3+b**3+c**3
#     if count_num == self_num:
#         math_self.append(self_num)
# print(math_self)

# 方法三：参考答案
# for num in range(100, 1000):
#     low = num % 10
#     mid = num // 10 % 10
#     high = num // 100
#     if num == low ** 3 + mid ** 3 + high ** 3:
#         print(num)

# 反转数字逻辑--自己捋
# a=234
# print(a % 10)
# print(a // 10 % 10)
# print(a //100)

# 反转数字
# num = int(input('num = '))
# reversed_num = 0
# while num > 0:
#     reversed_num = reversed_num * 10 + num % 10
#     num //= 10
# print(reversed_num)

# 生产斐波那契数列
# 方法一
# fei_list = []
# a = 0
# sum = 1
# while(len(fei_list)<20):
#     b = sum
#     sum += a
#     a = b
#     fei_list.append(a)
# print(fei_list)
# a=1
# b=2
# print(a,b)
# a,b=b,a+b
# print(a,b)

# 方法二：
# a = 0
# b = 1
# for _ in range(20):
#     a, b = b, a + b
#     print(a, end=' ')
# 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597 2584 4181 6765
# [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765]


# 找出10000以内的完美数。例如：6（$6=1+2+3$）和28（$28=1+2+4+7+14$）就是完美数
import math
# 方法一：自己写的
'''
有个两个小错误:
1、数字应该是没有1的，从2开始找
2、不加重复的因子，比如2*2=4，则只加一个2
'''
# per_list=[]
# for i in range(2,10000):
#     sum = 0
#     n = int(math.sqrt(i))
#     for j in range(1,n+1):
#         if(i%j==0):
#             sum += j
#             if j>1 and i//j!=j:
#                 sum+=i//j
#     if(sum==i):
#         per_list.append(i)
# print(per_list)

# 方法二：参考答案
# for num in range(2, 10000):
#     result = 0
#     for factor in range(1, int(math.sqrt(num)) + 1):
#         if num % factor == 0:
#             result += factor
#             # 这里的意思是不加重复的因子，比如2*2=4，则只加一个2
#             if factor > 1 and num // factor != factor:
#                 result += num // factor
#     if result == num:
#         print(num)


# 输出100以内所有的素数。
# 方法一：自己写的 [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
# su_list=[]
# for i in range(2,101):
#     flag = 1
#     if i<4:
#         su_list.append(i)
#     else:
#         n=int(math.sqrt(i))
#         for j in range(2,n+1):
#             if(i%j==0):
#                 flag=0
#                 break
#         if flag==1:
#             su_list.append(i)
# print(su_list)

# 方法二：参考答案 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97
# for num in range(2, 100):
#     is_prime = True
#     for factor in range(2, int(math.sqrt(num)) + 1):
#         if num % factor == 0:
#             is_prime = False
#             break
#     if is_prime:
#         print(num, end=' ')



